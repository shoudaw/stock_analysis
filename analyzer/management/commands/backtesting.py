from django.core.management.base import BaseCommand, CommandError
from analyzer.backtesting import UpInOneDayBackTesting


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    # def add_arguments(self, parser):
    #     # Positional arguments
    #     parser.add_argument('symbols', nargs='+', type=str)
    # #
    # #     # Named (optional) arguments
    # #     parser.add_argument(
    # #         '--delete',
    # #         action='store_true',
    # #         dest='delete',
    # #         default=False,
    # #         help='Delete poll instead of closing it',
    # #     )
    #
    def handle(self, *args, **options):
        # bt = TopToBuyBackTesting()
        bt = UpInOneDayBackTesting()
        bt.up_in_one_day_back_test_guide()

