import holidays
import pytz
import logging
import time as sys_time
from pytz import timezone
from django.core.management.base import BaseCommand, CommandError
from analyzer.tools import PullQuote
from dateutil import parser as date_parser
from bdateutil import isbday
from bdateutil import relativedelta
from datetime import datetime
from datetime import time



logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Pull Quotes'

    def handle(self, *args, **options):
        msg = 'Start'
        self.stdout.write(self.style.SUCCESS(msg))
        pq = PullQuote()
        pq.pull_one_minute_qoute_all_symbols()
        msg = 'Finish'
        self.stdout.write(self.style.SUCCESS(msg))
