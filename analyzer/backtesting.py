from barchart_signals.services import Service as BarchartDataService
from datetime import date
from datetime import datetime
from bdateutil import relativedelta
from bdateutil import isbday
from quote.models import OneMinuteQuote
import holidays
import re

class UpInOneDayBackTesting(object):
    barchart_data_service = BarchartDataService()
    start_date = date(2016, 7, 1)
    end_date = date(2016, 7, 15)
    def _get_data_on_date(self, signal, dt, path_fmt):
        if isinstance(dt, datetime):
            dt = datetime.date()
        elif isinstance(dt, date):
            pass
        else:
            raise ValueError
        path = path_fmt(signal, dt)
        res = self.barchart_data_service.get_data_by_path(path)
        return res

    def _get_data_on_date_signal(self, signal, dt):
        path_fmt = lambda signal, dt: 'signal/daily/{}/{}.json'.format(signal, dt)
        res = self._get_data_on_date(signal, dt, path_fmt)
        return res

    def _get_data_on_date_guide(self, signal, dt):
        path_fmt = lambda signal, dt: 'guide/daily/{}/{}.json'.format(signal, dt)
        res = self._get_data_on_date(signal, dt, path_fmt)
        return res

    def up_in_one_day_guide(self, signal, dt, limit=100):
        barchart = self._get_data_on_date_guide(signal, dt)
        symbols = [it['sym'] for it in barchart][:limit]
        ndt = dt + relativedelta(bdays=1, holidays=holidays.US())
        stats = []
        for sym in symbols:
            quotes = OneMinuteQuote.objects.filter(symbol=sym, date=ndt)
            if not quotes.exists():
                continue
            open_day = quotes.order_by('time').first().open
            close_day = quotes.order_by('time').last().close
            high_day = quotes.order_by('-high').first().high
            item = {
                'sym': sym,
                'open_day': open_day,
                'close_day': close_day,
                'high_day': high_day,
                'isUp': close_day > open_day,
                # 'isUp': high_day > open_day,
                # 'up_rate': (high_day - open_day) / open_day
                'up_rate_close': float(close_day - open_day) / float(open_day) * 100,
                'up_rate_high': float(high_day - open_day) / float(open_day) * 100,
            }
            stats.append(item)
        return stats

    def up_in_one_day_signal(self, signal, dt, limit=100):
        barchart = self._get_data_on_date_signal(signal, dt)
        symbols = [it['sym'] for it in barchart][:limit]
        ndt = dt + relativedelta(bdays=1, holidays=holidays.US())
        stats = []
        for sym in symbols:
            quotes = OneMinuteQuote.objects.filter(symbol=sym, date=ndt)
            if not quotes.exists():
                continue
            open_day = quotes.order_by('time').first().open
            close_day = quotes.order_by('time').last().close
            high_day = quotes.order_by('-high').first().high
            item = {
                'sym': sym,
                'open_day': open_day,
                'close_day': close_day,
                'high_day': high_day,
                'isUp': close_day > open_day,
                # 'isUp': high_day > open_day,
                # 'up_rate': (high_day - open_day) / open_day
                'up_rate_close': float(close_day - open_day) / float(open_day) * 100,
                'up_rate_high': float(high_day - open_day) / float(open_day) * 100,
            }
            stats.append(item)
        return stats

    def up_in_one_day_back_test_guide(self):
        guide_daily = [
            'guide_min',
            'price_vs_twenty_day_moving_average',
            'seven_day_directional_buy',
            'ten_eight_moving_average',
            'today_max',
            'trend_spotter_buy',
            'twenty_day_bollinger_bands',
            'twenty_fifty_day_macd',
        ]
        for signal in guide_daily:
            stats = []
            for delta in range(15):
                dt = self.start_date + relativedelta(day=delta, holidays=holidays.US())
                if isbday(dt, holidays=holidays.US()):
                    stats_one_day = self.up_in_one_day_guide(signal, dt, 1000)
                    item = {
                        'date': dt.isoformat(),
                        'stats_one_day': stats_one_day,
                    }
                    stats.append(item)
            days = len(stats)
            stitle = tuple([it['date'] for it in stats for _ in (0, 1)])
            from plotly import tools
            fig = tools.make_subplots(rows=days, cols=2, subplot_titles=stitle)
            row_idx = 1
            for it in stats:
                dt = it['date']
                stats_one_day = it['stats_one_day']
                plotly_data_1 = self._get_plotly_bar_chart_data(stats_one_day, 'sym', 'up_rate_close')
                fig.append_trace(plotly_data_1, row_idx, 1)
                plotly_data_1 = self._get_plotly_bar_chart_data(stats_one_day, 'sym', 'up_rate_high')
                fig.append_trace(plotly_data_1, row_idx, 2)
                row_idx += 1
            fig['layout'].update(
                autosize=True,
                width=2048,
                height=4000,
                title=signal,
            )
            for layout_key in fig['layout'].keys():
                m = re.match('yaxis(\d+)', layout_key)
                if m is not None:
                    y_idx = int(m.group(1))
                    y_range = [-5, 5]
                    if y_idx % 2 == 0:
                        y_range = [0, 5]
                    fig['layout'][layout_key].update(range=y_range)

            import plotly.offline as py_offline
            # chart = py.iplot(fig, filename=signal)
            chart = py_offline.plot(fig, filename=signal)
            import ipdb; ipdb.set_trace()
            pass

    def up_in_one_day_back_test_signal(self):
        signals = [
            'top_stocks_to_buy',
            'bottom_stocks_to_buy',
            'top_signal_direction',
            'top_signal_strength',
            'upgrades',
            'downgrades',
        ]
        for signal in signals:
            stats = []
            for delta in range(15):
                dt = self.start_date + relativedelta(day=delta, holidays=holidays.US())
                if isbday(dt, holidays=holidays.US()):
                    stats_one_day = self.up_in_one_day_signal(signal, dt, 1000)
                    item = {
                        'date': dt.isoformat(),
                        'stats_one_day': stats_one_day,
                    }
                    stats.append(item)
            days = len(stats)
            stitle = tuple([it['date'] for it in stats for _ in (0, 1)])
            from plotly import tools
            fig = tools.make_subplots(rows=days, cols=2, subplot_titles=stitle)
            row_idx = 1
            for it in stats:
                dt = it['date']
                stats_one_day = it['stats_one_day']
                plotly_data_1 = self._get_plotly_bar_chart_data(stats_one_day, 'sym', 'up_rate_close')
                fig.append_trace(plotly_data_1, row_idx, 1)
                plotly_data_1 = self._get_plotly_bar_chart_data(stats_one_day, 'sym', 'up_rate_high')
                fig.append_trace(plotly_data_1, row_idx, 2)
                row_idx += 1
            fig['layout'].update(
                autosize=True,
                width=2048,
                height=4000,
                title=signal,
            )
            for layout_key in fig['layout'].keys():
                m = re.match('yaxis(\d+)', layout_key)
                if m is not None:
                    y_idx = int(m.group(1))
                    y_range = [-5, 5]
                    if y_idx % 2 == 0:
                        y_range = [0, 5]
                    fig['layout'][layout_key].update(range=y_range)

            import plotly.offline as py_offline
            # chart = py.iplot(fig, filename=signal)
            chart = py_offline.plot(fig, filename=signal)
            import ipdb; ipdb.set_trace()
            pass

    def _get_plotly_bar_chart_data(self, data, x_key, y_key):
        import plotly.plotly as py
        import plotly.graph_objs as go
        x = [it[x_key] for it in data]
        y = [it[y_key] for it in data]
        plotly_data = go.Bar(x=x, y=y)
        return plotly_data


class BackTesting(object):
    def _get_data_on_date(self):
        raise NotImplementedError()

    def up_in_one_day(self, dt, limit=100):
        barchart = self._get_data_on_date(dt)
        symbols = [it['sym'] for it in barchart][:limit]
        ndt = dt + relativedelta(bdays=1, holidays=holidays.US())
        stats = []
        # quotes = OneMinuteQuote.objects.filter(symbol__in=symbols, date=ndt)
        for sym in symbols:
            quotes = OneMinuteQuote.objects.filter(symbol=sym, date=ndt)
            if not quotes.exists():
                continue
            open_day = quotes.order_by('time').first().open
            close_day = quotes.order_by('time').last().close
            high_day = quotes.order_by('-high').first().high
            item = {
                'sym': sym,
                'open_day': open_day,
                'close_day': close_day,
                'high_day': high_day,
                'isUp': close_day > open_day,
                # 'isUp': high_day > open_day,
                # 'up_rate': (high_day - open_day) / open_day
                'up_rate': (close_day - open_day) / open_day
            }
            stats.append(item)
        # ups = [item for item in stats if item['isUp']]
        # rate = len(ups) / float(len(symbols))
        # return rate
        return ['%2.2f%%' % (x['up_rate'] * 100) for x in stats]


    def up_in_one_day_back_test(self):
        # dt = self.start_date + relativedelta(bdays=6, holidays=holidays.US())
        # dt = self.start_date
        stats = []
        for delta in range(15):
            dt = self.start_date + relativedelta(day=delta, holidays=holidays.US())
            if isbday(dt, holidays=holidays.US()):
                rate = self.up_in_one_day(dt, 10)
                item = {
                    'date': dt,
                    'rate': rate,
                }
                stats.append(item)
        import ipdb; ipdb.set_trace()
        pass

