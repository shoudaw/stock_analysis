from google_finance.services import GoogleIntradayQuoteService
from quote.models import OneMinuteQuote
from quote.models import SymbolMeta

import holidays
import pytz
import logging
import time as sys_time
from pytz import timezone
from django.core.management.base import BaseCommand, CommandError
from dateutil import parser as date_parser
from bdateutil import isbday
from bdateutil import relativedelta
from datetime import datetime
from datetime import time
import random

from proxicity.services import ProxicityServices


logger = logging.getLogger(__name__)

class PullQuote(object):
    def __init__(self):
        self.count_1 = random.randint(5, 15)
        self.count_2 = random.randint(100, 300)

    def get_latest_market_date(self):
        tz = timezone('US/Eastern')
        now = datetime.now(tz)
        now_time = now.time()
        now_date = now.date()
        market_close_time = time(16, 0, 0)
        market_date = now_date if now_time > market_close_time else now_date - relativedelta(days=1)
        market_date = market_date if isbday(market_date) else market_date - relativedelta(bdays=1)
        return market_date

    def pull_one_minute_quote(self, symbol, period=15, proxies=None):
        try:
            latest = OneMinuteQuote.objects.filter(symbol=symbol).latest('date')
        except OneMinuteQuote.DoesNotExist:
            latest = None

        if latest is not None:
            latest_date = latest.date
            market_date = self.get_latest_market_date()

            delta = relativedelta(market_date, latest_date, holidays=holidays.US())
            period = delta.bdays
        else:
            period = 15

        if period == 0:
            return (None, None)

        google_intraday_quote_service = GoogleIntradayQuoteService(symbol=symbol, interval=60, period=period, proxies=proxies)
        lst = google_intraday_quote_service.get_quotes()
        if not lst:
            return (None, None)
        quotes = [OneMinuteQuote(**it) for it in lst]
        start_date, end_date = quotes[0].date, quotes[-1].date
        try:
            OneMinuteQuote.objects.bulk_create(quotes)
        except:
            quotes = []
            prev_date = None
            for it in lst:
                query = {
                    'date': it['date'],
                    'time': it['time'],
                    'symbol': it['symbol'],
                }
                if prev_date == it['date']:
                    continue
                if OneMinuteQuote.objects.filter(**query).exists():
                    prev_date = it['date']
                    continue
                prev_date = None
                quotes.append(OneMinuteQuote(**it))
            OneMinuteQuote.objects.bulk_create(quotes)
        return start_date, end_date

    def pull_one_minute_qoute_all_symbols(self):
        market_date = self.get_latest_market_date()
        symbolMetas = SymbolMeta.objects.filter(last_update_time__lt=market_date).order_by('id')
        tz = timezone('US/Eastern')
        count = 0
        success_count = 0
        proxicity_services = ProxicityServices()
        proxies = None
        # proxies = proxicity_services.get_proxy()
        # print proxies
        logger.info('Start: total_symbol_count:{}'.format(symbolMetas.count()))
        for symbol_meta in symbolMetas:
            count += 1
            skip = False
            symbol = symbol_meta.symbol
            start_date, end_date = self.pull_one_minute_quote(symbol, proxies=proxies)
            if end_date is not None:
                update_time = datetime.now(tz)
                symbol_meta.last_update_time = update_time
                symbol_meta.save()
                success_count += 1
            else:
                skip = True

            logger.info('Symbol:{}, start_date:{}, end_date:{}, last_update_time:{}'.format(
                symbol, start_date, end_date, symbol_meta.last_update_time
            ))
            self._random_sleep(skip=skip)
            # if success_count % 30 == 0:
            #     proxies = proxicity_services.get_proxy()

        logger.info('Finish, success_count:{}'.format(success_count))

    def _random_sleep(self, skip=False):
        if skip:
            return
        if self.count_1 <= 0:
            self.count_1 = random.randint(5, 15)
            logger.info('Sleep {}s'.format(2 * self.count_1))
            sys_time.sleep(2 * self.count_1)
        if self.count_2 <= 0:
            self.count_2 = random.randint(100, 300)
            logger.info('Sleep {}s'.format(5 * self.count_2))
            sys_time.sleep(5 * self.count_1)
        self.count_1 -= 1
        self.count_2 -= 1

    def pull_one_minute_quote_bak(self, symbol, period=15):
        google_intraday_quote_service = GoogleIntradayQuoteService(symbol=symbol, interval=60, period=period)
        lst = google_intraday_quote_service.get_quotes()
        if not lst:
            return (None, None)
        quotes = [OneMinuteQuote(**it) for it in lst]
        start_date, end_date = quotes[0].date, quotes[-1].date
        try:
            pass
            OneMinuteQuote.objects.bulk_create(quotes)
        except:
            quotes = []
            prev_date = None
            for it in lst:
                query = {
                    'date': it['date'],
                    'time': it['time'],
                    'symbol': it['symbol'],
                }
                if prev_date == it['date']:
                    continue
                if OneMinuteQuote.objects.filter(**query).exists():
                    prev_date = it['date']
                    continue
                prev_date = None
                quotes.append(OneMinuteQuote(**it))
            OneMinuteQuote.objects.bulk_create(quotes)
        return start_date, end_date

