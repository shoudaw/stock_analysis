from django.template.loader import render_to_string
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (
    Mail, Attachment, FileContent, FileName,
    FileType, Disposition, ContentId)
from stocktwits.tradingview_widget import TradingViewWidget
import logging


logger = logging.getLogger(__name__)


class Notification(object):

    @classmethod
    def email_tradingview_widgets(cls, symbols):
        subject = f"New symbols found: {', '.join(symbols)}"
        to_addresses = ['shoudaw+stock_analysis_alert@gmail.com']
        from_email = "trending_stock@localhost"

        body = render_to_string("email/trending_stock.html", {"symbols": symbols})
        message = Mail(
            from_email=from_email,
            to_emails=to_addresses,
            subject=subject,
            html_content=body)

        for symbol in symbols:
            attachment = Attachment()
            cid = f'{symbol}'
            encoded_string = TradingViewWidget.img_base64str_advance_charts(symbol)
            attachment.file_content = FileContent(encoded_string)
            attachment.file_type = FileType('image/jpeg')
            attachment.file_name = FileName(f'{cid}.jpeg')
            attachment.disposition = Disposition('inline')
            attachment.content_id = ContentId(f'{cid}')
            message.add_attachment(attachment)

        try:
            sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
            sg.send(message)
            logger.info(f'Email sent, {subject}, {to_addresses}')
        except Exception as e:
            logger.info(f'Email failed, {e}')
            raise e
