from datetime import datetime
from django.shortcuts import render

# Create your views here.

from django.http import Http404
from django.shortcuts import render
from stocktwits.clients import api
from stocktwits.models import TrendingStock
import pytz


def trending_stocks(request):
    symbols = api.get_trending_stocks()
    return render(request, 'trending_stocks.html', {'symbols': symbols})


def trending_stocks_advance(request):
    symbols = api.get_trending_stocks()
    now = datetime.utcnow().replace(tzinfo=pytz.utc)
    for symbol in symbols:
        symbol['db_created_at'] = now
    return render(request, 'trending_stocks_advance.html', {'symbols': symbols})


def trending_stocks_advance_recent(request, limit=30):
    stocks = TrendingStock.objects.filter().order_by('-db_created_at')[:limit]
    return render(request, 'trending_stocks_advance.html', {'symbols': stocks})
