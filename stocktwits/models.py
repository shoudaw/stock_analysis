from django.db import models
from django.contrib.postgres import fields
from enum import Enum


class Sentiment(Enum):
    BEARISH = 'bearish'
    BULLISH = 'bullish'


class Twit(models.Model):
    objects = models.Manager()

    class Meta:
        index_together = [
            ["symbol", "twit_created_at"],
            ["symbol", "db_created_at"],
        ]

    id = models.AutoField(primary_key=True)
    twit_id = models.IntegerField(null=True)
    twit_user_id = models.IntegerField(null=True)
    twit_created_at = models.DateTimeField(db_index=True)
    symbol = models.CharField(max_length=20, db_index=True)
    sentiment = models.CharField(max_length=10, choices=[(tag.value, tag.value) for tag in Sentiment], null=True)
    body = models.TextField(null=True)
    metadata = fields.JSONField(null=True)
    raw = fields.JSONField(null=True)
    db_created_at = models.DateTimeField(db_index=True, auto_now_add=True)

    def __str__(self):
        return f'{{twit_id: {self.twit_id}, symbol: {self.symbol}, sentiment: {self.sentiment}, user_id: {self.twit_user_id}}}'


class TrendingStock(models.Model):
    id = models.AutoField(primary_key=True)
    stock_id = models.IntegerField(null=True)
    symbol = models.CharField(max_length=20, db_index=True)
    title = models.CharField(max_length=200, null=True)
    watchlist_count = models.IntegerField(null=True)
    raw = fields.JSONField(null=True)
    db_created_at = models.DateTimeField(db_index=True, auto_now_add=True)
