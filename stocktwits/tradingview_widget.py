from django.template.loader import render_to_string
from tempfile import NamedTemporaryFile
from selenium import webdriver


class TradingViewWidget(object):

    SCRIPT_TIME_SECOND = 10
    @classmethod
    def html_advance_charts(cls, symbol: str):
        symbol = symbol.upper()
        template_name = "tradingview/advance_chart.html"
        html = render_to_string(
            template_name,
            {
                "symbol": symbol,
            }
        )
        return html

    @classmethod
    def img_base64str_advance_charts(cls, symbol: str) -> str:
        symbol = symbol.upper()
        html = cls.html_advance_charts(symbol)
        options = webdriver.FirefoxOptions()
        options.headless = True
        driver = webdriver.Firefox(options=options)

        with NamedTemporaryFile(suffix=".html", delete=False) as file:
            file.write(html.encode())
            file.flush()
            driver.implicitly_wait(cls.SCRIPT_TIME_SECOND)
            driver.set_script_timeout(cls.SCRIPT_TIME_SECOND)
            driver.get(f"file://{file.name}")
            element = driver.find_element_by_tag_name('html')
            encoded_string = element.screenshot_as_base64
            driver.quit()
            return encoded_string
