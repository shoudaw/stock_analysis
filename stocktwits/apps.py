from django.apps import AppConfig


class StocktwitsConfig(AppConfig):
    name = 'stocktwits'
