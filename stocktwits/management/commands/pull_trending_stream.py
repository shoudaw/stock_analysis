from django.core.management.base import BaseCommand, CommandError
from stocktwits.models import Twit, Sentiment
from stocktwits.clients import api
import time
from dateutil import parser as dt_parser
from stocktwits.notification import Notification


def safe_get(dct, *keys):
    for key in keys:
        try:
            dct = dct[key]
        except (KeyError, TypeError):
            return None
    return dct


class Command(BaseCommand):
    help = 'pull and watch the trending stream'
    SLEEP = 180

    def add_arguments(self, parser):
        # parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        prev = None
        while True:
            resp = api.get_trending_stream()
            pulled_symbols = set()
            status = safe_get(resp, 'response', 'status')
            if status != 200:
                time.sleep(self.SLEEP)
                self.stdout.write(self.style.ERROR(f'ERROR, status: {status}'))
                continue

            msgs = resp['messages']
            for msg in msgs:
                symbols = msg.get('symbols')
                # ignore the twit that has multiple symbolx
                if len(symbols) > 1:
                    continue
                pulled_symbols.update(x['symbol'] for x in symbols)
                twit_id = msg.get('id')
                qs = Twit.objects.filter(pk=twit_id)
                if qs.exists():
                    continue
                twit = Twit()
                twit.twit_id = twit_id
                twit.body = msg.get('body')
                twit.symbol = symbols[0]['symbol']
                sentiment = safe_get(msg, 'entities', 'sentiment', 'basic')
                if sentiment:
                    sentiment = sentiment.lower()
                    if sentiment == Sentiment.BEARISH.value:
                        twit.sentiment = Sentiment.BEARISH.value
                    elif sentiment == Sentiment.BULLISH.value:
                        twit.sentiment = Sentiment.BULLISH.value
                    else:
                        # log error
                        self.stdout.write(self.style.ERROR(f'got sentiment {sentiment}'))
                        pass
                user_id = safe_get(msg, 'user', 'id')
                twit.twit_user_id = user_id
                twit.raw = msg
                created_at_str = msg.get('created_at')

                created_at = None
                if created_at_str:
                    created_at = dt_parser.parse(created_at_str)
                twit.twit_created_at = created_at
                twit.save()
                self.stdout.write(f'Save new twit: {twit}')
            if prev is None:
                prev = pulled_symbols

            new_symbols = pulled_symbols - prev
            prev = pulled_symbols

            if new_symbols:
                try:
                    Notification.email_tradingview_widgets(new_symbols)
                    self.stdout.write(self.style.SUCCESS(f'Email sent, {new_symbols}'))
                except Exception as e:
                    self.stdout.write(self.style.ERROR(f'Error sending email {e}'))
            time.sleep(self.SLEEP)

