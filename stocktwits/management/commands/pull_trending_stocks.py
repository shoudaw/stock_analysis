from django.core.management.base import BaseCommand
from stocktwits.clients import api
from stocktwits.notification import Notification
from stocktwits.models import TrendingStock
from dateutil import parser as dt_parser
from datetime import datetime
import time
import logging


logger = logging.getLogger(__name__)


def safe_get(dct, *keys):
    for key in keys:
        try:
            dct = dct[key]
        except (KeyError, TypeError):
            return None
    return dct


class Command(BaseCommand):
    help = 'pull and watch the trending stream'
    SLEEP = 180
    TOP_N = 30

    def add_arguments(self, parser):
        # parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        # self.notify(['AAPL', "NVDA", "AMZN"])
        # return
        email_notification = False
        start_time = dt_parser.parse("5:30am").time()
        end_time = dt_parser.parse("1:30pm").time()
        prev = None
        while True:
            now = datetime.now().time()
            try:
                resp = api.get_trending_stocks()[:self.TOP_N]
                stock_dct = {stock_msg['symbol']: stock_msg for stock_msg in resp}
                symbols = set(stock_dct.keys())
                self.stdout.write(f'Got symbols: {symbols}')
                if not prev:
                    prev = symbols
                    continue
                new_symbols = symbols - prev
                prev = symbols
                if new_symbols:
                    for symbol in new_symbols:
                        stock = stock_dct[symbol]
                        trending_stock = TrendingStock()
                        trending_stock.symbol = stock['symbol']
                        trending_stock.title = stock['title']
                        trending_stock.stock_id = stock['id']
                        trending_stock.watchlist_count = stock['watchlist_count']
                        trending_stock.raw = stock
                        trending_stock.save()
                    logger.info(f"New symbols: {new_symbols}")
                    if email_notification:
                        if start_time < now < end_time:
                            Notification.email_tradingview_widgets(new_symbols)
                            self.stdout.write(self.style.SUCCESS(f'Email sent, {new_symbols}'))
                        else:
                            self.stdout.write(self.style.SUCCESS(f'New symbols, {new_symbols}'))
                    else:
                        self.stdout.write(self.style.SUCCESS(f'New symbols, {new_symbols}'))
            except Exception as e:
                logging.error(f"Error:{e}")
                self.stdout.write(self.style.ERROR(f"Error, {e}"))
                continue
            finally:
                time.sleep(self.SLEEP)
