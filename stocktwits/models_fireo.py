from enum import Enum
from datetime import datetime

from fireo.models import Model
from fireo.fields import TextField
from fireo.fields.id_field import IDField
from fireo.fields.data_time import DateTime
from fireo.fields.map_field import MapField
from fireo.fields.number_field import NumberField
from fireo.fields.text_field import TextField

# class User(Model):
#     name = TextField()


# u = User()
# u.name = "Azeem Haider"
# u.save()

# # Get user
# user = User.collection.get(u.key)
# print(user.name)

class Sentiment(Enum):
    BEARISH = 'bearish'
    BULLISH = 'bullish'


class TrendingStock(Model):
    id = IDField()
    stock_id = NumberField()
    symbol = TextField()
    title = TextField()
    watchlist_count = NumberField()
    raw = MapField()
    db_created_at = DateTime()

    def save(self, *args, **kwargs):
        if not self.db_created_at:
            self.db_created_at = datetime.utcnow()

        if not self.id:
            self.id = f"{int(self.db_created_at.timestamp())}-{self.symbol}"

        return super().save(*args, **kwargs)
