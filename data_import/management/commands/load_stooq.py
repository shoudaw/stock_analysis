from django.core.management.base import BaseCommand, CommandError
from data_import.stooq import Stooq

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    # def add_arguments(self, parser):
        # Positional arguments
        # parser.add_argument('symbols', nargs='+', type=str)

        # Named (optional) arguments
        # parser.add_argument(
        #     '--root',
        #     action='store_true',
        #     dest='delete',
        #     default=False,
        #     help='Delete poll instead of closing it',
        # )

    def handle(self, *args, **options):
        stooq = Stooq()
        path = '/data/tmp/data/'
        stooq.load_all(path)

        # root = config.DATA_ROOT
        # file_paths = []
        # for path, subdirs, files in os.walk(root):
        #     for name in files:
        #         if fnmatch.fnmatch(name, '*.json'):
        #             file_path = os.path.join(path, name)
        #             file_paths.append(file_path)
        #
        # all_symbols = set()
        # for file_path in file_paths:
        #     with open(file_path, 'r') as f:
        #         data = json.load(f)
        #         symbols = [x['sym'] for x in data]
        #     all_symbols.update(symbols)
        # all_symbols = sorted(all_symbols)
        # with open('/data/tmp/symbols.json', 'w+') as f:
        #     json.dump(all_symbols, f, indent=4)
        # with open('/data/tmp/symbols.txt', 'w+') as f:
        #     for sym in all_symbols:
        #         f.write(sym + '\n')
        #         print sym
