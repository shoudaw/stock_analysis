import os.path
import fnmatch
import json
import re


class Stooq(object):

    def parse(self, raw):
        pass

    def load_all(self, root):
        pass
        file_paths = []
        for path, subdirs, files in os.walk(root):
            for name in files:
                if fnmatch.fnmatch(name, '*.us.txt'):
                    file_path = os.path.join(path, name)
                    file_paths.append(file_path)
        symbols = []
        for file_path in file_paths:
            name = os.path.basename(file_path)
            # m = re.match('([a-zA-Z_\-]+).us.txt', name)
            m = re.match('([a-zA-Z]+).us.txt', name)
            if m:
                symbol = m.group(1)
                # symbol = symbol.replace('-', '.')
                # symbol = symbol.replace('_', '-')
            else:
                print name
                continue
            with open(file_path, 'r') as f:
                data = self.parse(f.readline())
            symbols.append(symbol)


        import ipdb; ipdb.set_trace()
        pass

        # all_symbols = set()
        # for file_path in file_paths:
        #     with open(file_path, 'r') as f:
        #         data = json.load(f)
        #         symbols = [x['sym'] for x in data]
        #     all_symbols.update(symbols)
        # all_symbols = sorted(all_symbols)
        # with open('/data/tmp/symbols.json', 'w+') as f:
        #     json.dump(all_symbols, f, indent=4)
        # with open('/data/tmp/symbols.txt', 'w+') as f:
        #     for sym in all_symbols:
        #         f.write(sym + '\n')
        #         print sym
