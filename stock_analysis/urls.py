"""stock_analysis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path, re_path
from django.contrib import admin
from stocktwits.views import trending_stocks
from stocktwits.views import trending_stocks_advance
from stocktwits.views import trending_stocks_advance_recent
from stock_data.views import pull_intraday
from stock_data.views import pull_tasks_status
from stock_data.views import enqueue_pull_task
from stock_data.views import enqueue_pull_ticker
from stock_data.views import pull_tickers
from stock_data.views import pull_trending_stocks

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    path(r'trending_stocks/', trending_stocks),
    path(r'trending_stocks_advance/', trending_stocks_advance),
    path(r'trending_stocks_advance/recent/', trending_stocks_advance_recent),
    path(r'trending_stocks_advance/recent/<int:limit>/', trending_stocks_advance_recent),
    path(r'pull_intraday', pull_intraday),
    re_path(r'pull_intraday/(?P<date_str>[0-9]{4}-?[0-9]{2}-?[0-9]{2})/$', pull_intraday),
    path(r'pull_tasks_status', pull_tasks_status),
    re_path(r'enqueue_pull_task/(?P<date_str>[0-9]{4}-?[0-9]{2}-?[0-9]{2})/$', enqueue_pull_task),
    path(r'enqueue_pull_ticker', enqueue_pull_ticker),
    path(r'pull_tickers', pull_tickers),
    path(r'pull_trending_stocks', pull_trending_stocks),
]
