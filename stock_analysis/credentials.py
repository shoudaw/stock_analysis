import os
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# shoudaw@gmail.com
# GCP_PROJECT_ID = "stock-analysis-255821"

# johnnywsd@gmail.com
GCP_PROJECT_ID = "stock-analysis-271305"

if os.getenv('GAE_APPLICATION', None):
  cred = credentials.ApplicationDefault()

  firebase_admin.initialize_app(cred, {
    'projectId': GCP_PROJECT_ID,
  })
  db = firestore.client()
  ALPHA_VANTAGE_API_KEY = os.environ.get("ALPHA_VANTAGE_API_KEY", '')
  ALPACA_KEY_ID = os.environ.get("ALPACA_KEY_ID", '') or db.collection('credentials').document(u'ALPACA_KEY_ID').get().to_dict()['value']
  ALPACA_SECRET_KEY = os.environ.get("ALPACA_SECRET_KEY", '') or db.collection('credentials').document(u'ALPACA_SECRET_KEY').get().to_dict()['value']
else:
  ALPHA_VANTAGE_API_KEY = os.getenv("ALPHA_VANTAGE_API_KEY", None)
  ALPACA_KEY_ID = os.getenv("ALPACA_KEY_ID", None)
  ALPACA_SECRET_KEY = os.getenv("ALPACA_SECRET_KEY", None)
