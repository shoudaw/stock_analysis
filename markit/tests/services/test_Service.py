from datetime import datetime
from django.test import TestCase
from markit.services import Service

# Create your tests here.
class GetHistoryTest(TestCase):
    def test_happy_path(self):
        service = Service()
        start_date = datetime(2016, 7, 5)
        end_date = datetime(2016, 7, 8)
        service.get_history('AAPL', start_date, end_date)
        pass
