import json
import requests

ROOT = 'http://dev.markitondemand.com/Api/v2/'
CHART_END_POINT = ROOT + 'InteractiveChart/jsonp'


class Service(object):
    def _get_history(self, symbol, start_date, end_date,
                    type='price', params='ohlc', data_period='Minute'):
        elements = [
            {
                'Symbol': symbol,
                'Type': type,
                'Params': ['c']
            }
        ]
        start_date_str = start_date.isoformat()
        end_date_str = end_date.isoformat()
        api_input = {
            'Normalized': False,
            'DataPeriod': data_period,
            'Elements': elements,
            'NumberOfDays': 2,
            'DataInterval': 1,
            # 'StartDate': start_date_str,
            'EndDate': end_date_str,
        }
        payload = {
            'parameters': json.dumps(api_input)
        }
        res = requests.get(CHART_END_POINT, params=payload)
        import ipdb; ipdb.set_trace()
        pass

