import alpaca_trade_api as tradeapi
from stock_analysis.credentials import ALPACA_KEY_ID
from stock_analysis.credentials import ALPACA_SECRET_KEY
import csv

api = tradeapi.REST(ALPACA_KEY_ID, ALPACA_SECRET_KEY, api_version='v2')
account = api.get_account()

symbols = []
with open("/Users/shoudawang/projects/stock_analysis/scanner/data/stocks_momentum.csv", "r") as input_file:
    csv_reader = csv.reader(input_file)
    # Skip the first line
    next(csv_reader)

    for row in csv_reader:
        symbols.append(row[0])

import pandas_market_calendars as mcal
nyse = mcal.get_calendar('NYSE')
nyse.schedule(start_date='2012-07-01', end_date='2012-07-10')


recent_mintue_bars = {}
for i in range(0, 100, len(symbols)):
    recent_mintue_bars.update(api.get_barset(symbols[i: i + 100], "1Min", 330 * 2))
symbols = list(recent_mintue_bars.keys())


from datetime import datetime, time
today = datetime.today().date()
today_start_time = datetime.combine(today, time.min)

one_bars = next(iter(recent_mintue_bars.values()))
# TODO: use calendar get the last trading date
last_timestamp = one_bars.df.loc[:today_start_time].iloc[-1].name
last_trading_date = last_timestamp.to_pydatetime().date()


for symbol in symbols:
    try:
        last_trade = last_trades[symbol]
        bars = recent_mintue_bars[symbol].df.loc[last_trading_date:]
        change = last_trade.price / bars.iloc[-1].close - 1
        if change > 0.04:
            print(f"symbol: {symbol}, change: {change}")
    except Exception as e:
        print(f"symbol: {symbol}, e: {e}")

