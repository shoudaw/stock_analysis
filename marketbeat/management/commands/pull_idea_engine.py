from django.core.management.base import BaseCommand
from stocktwits.clients import api
from stocktwits.notification import Notification
from stocktwits.models import TrendingStock
from dateutil import parser as dt_parser
from datetime import datetime
import time
import logging
import requests


logger = logging.getLogger(__name__)


def safe_get(dct, *keys):
    for key in keys:
        try:
            dct = dct[key]
        except (KeyError, TypeError):
            return None
    return dct


class Command(BaseCommand):
    help = 'pull and watch the trending stream'
    SLEEP = 180
    TOP_N = 30

    cookies = {}
    headers = {}

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        requests.Session()

        from dateutil.relativedelta import relativedelta
        from dateutil.relativedelta import MO
        from dateutil.parser import parse

        end_date = parse("2019-4-22")
        for i in range(105):
            dt = end_date - relativedelta(weekday=MO(-i))
            dt_str = dt.date().isoformat()
            filename = self.get_filename(dt_str)
            with open(filename, "w+") as file:
                resp = requests.get(
                    f"https://www.marketbeat.com/ratingsdb/idea-engine/{dt_str}/",
                    headers=self.headers,
                    cookies=self.cookies,
                )
                file.write(resp.text)
                self.stdout.write(filename)

    def get_filename(self, dt_str):
        BASE_DIR = "/media/johnny/SG4/stock_data/marketbeat/idea_engine/raw/"
        return f"{BASE_DIR}{dt_str}.html"
