import json
import os.path
from barchart_signals import config


class Service(object):
    def get_data_by_path(self, path):
        absolute_path = os.path.join(config.DATA_ROOT, path)
        with open(absolute_path, 'r') as f:
            res = json.load(f)
        return res

