import unittest
from mock import MagicMock
from barchart_signals import Scraper


class MockSessionFactory(object):
    def __init__(self, file_path):
        self.file_path = file_path
        pass

    def get_session(self):
        mock_session = MagicMock()
        with open(self.file_path, 'r') as f:
            text = f.readlines()
        mock_session.get.return_value = text


class GuideMinScraperTest(unittest.TestCase):
    def test(self):
        pass

#
# if __name__ == '__main__':
#     unittest.main(verbosity=2)

