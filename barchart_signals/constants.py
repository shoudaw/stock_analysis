ROOT_URL = 'http://www.barchart.com/'
_signal_formator = lambda *args, **kwargs: ROOT_URL + 'stocks/signals/{}?mode=D&view='.format(*args, **kwargs)

# TOP_STOCKS_TO_BUY_URL = ROOT_URL + 'stocks/signals/top100?mode=D&view='
# BOTTOM_STOCKS_TO_BUY_URL = ROOT_URL + 'stocks/signals/bottom100?mode=D&view='
# TOP_SIGNAL_STRENGTH = ROOT_URL + 'stocks/signals/strength?mode=D&view='
# TOP_SIGNAL_DIRECTION = ROOT_URL + 'stocks/signals/direction?mode=D&view='
# UPGRADES = ROOT_URL + 'stocks/signals/upgrades?mode=D&view='
# DOWNGRADES = ROOT_URL + 'stocks/signals/downgrades?mode=D&view='

TOP_STOCKS_TO_BUY_ID = 'top100'
BOTTOM_STOCKS_TO_BUY_ID = 'bottom100'
TOP_SIGNAL_STRENGTH_ID = 'strength'
TOP_SIGNAL_DIRECTION_ID = 'direction'
UPGRADES_ID = 'upgrades'
DOWNGRADES_ID = 'downgrades'

SIGNAL_NAME_ID_DCT = {
    'top_stocks_to_buy': TOP_STOCKS_TO_BUY_ID,
    'bottom_stocks_to_buy': BOTTOM_STOCKS_TO_BUY_ID,
    'top_signal_strength': TOP_SIGNAL_STRENGTH_ID,
    'top_signal_direction': TOP_SIGNAL_DIRECTION_ID,
    'upgrades': UPGRADES_ID,
    'downgrades': DOWNGRADES_ID,
}
SIGNAL_PREIODS = ['daily']
SIGNAL_DCT = {}
for period in SIGNAL_PREIODS:
    SIGNAL_DCT[period] = {k: _signal_formator(v) for k, v in SIGNAL_NAME_ID_DCT.iteritems()}
# SIGNAL_NAME_ID_DCT = {
#     'top_stocks_to_buy': _signal_formator(TOP_STOCKS_TO_BUY_ID),
#     'bottom_stocks_to_buy': _signal_formator(BOTTOM_STOCKS_TO_BUY_ID),
#     'top_signal_strength': _signal_formator(TOP_SIGNAL_STRENGTH_ID),
#     'top_signal_direction': _signal_formator(TOP_SIGNAL_DIRECTION_ID),
#     'upgrades': _signal_formator(UPGRADES_ID),
#     'downgrades': _signal_formator(DOWNGRADES_ID),
# }

_guide_period_formator = lambda x: 'mode={}&dwm={}'.format(x[0].upper(), x[0].lower())
_guide_formator = lambda *args, **kwargs: ROOT_URL + 'stocks/signals/guide/{}?{}&_dtp1=0'.format(*args, **kwargs)
TODAY_MAX_ID = 'MAX'
TODAY_MIN_ID = 'MIN'
TREND_SPOTTER_BUY_ID = '01B'
SEVEN_DAY_DIRECTIONAL_BUY_ID = '02B'
TEN_EIGHT_MOVING_AVERAGE_ID = '03B'
PRICE_VS_TWENTY_DAY_MOVING_AVERAGE_ID = '04B'
TWENTY_FIFTY_DAY_MACD_ID = '05B'
TWENTY_DAY_BOLLINGER_BANDS_ID = '06B'

GUIDE_NAME_ID_DCT = {
    'today_max': TODAY_MAX_ID,
    'guide_min': TODAY_MIN_ID,
    'trend_spotter_buy': TREND_SPOTTER_BUY_ID,
    'seven_day_directional_buy': SEVEN_DAY_DIRECTIONAL_BUY_ID,
    'ten_eight_moving_average': TEN_EIGHT_MOVING_AVERAGE_ID,
    'price_vs_twenty_day_moving_average': PRICE_VS_TWENTY_DAY_MOVING_AVERAGE_ID,
    'twenty_fifty_day_macd': TWENTY_FIFTY_DAY_MACD_ID,
    'twenty_day_bollinger_bands': TWENTY_DAY_BOLLINGER_BANDS_ID,
}
PREIODS = ['daily', 'weekly', 'monthly']

# GUIDE_DCT = {
#     'daily': {
#         'today_max': 'THE_URL'
#         'today_MIN': 'THE_URL'
#     },
#     'weekly': {},
#     'monthly': {},
# }
GUIDE_DCT = {}
for period in PREIODS:
    GUIDE_DCT[period] = {k: _guide_formator(v, _guide_period_formator(period)) for k, v in GUIDE_NAME_ID_DCT.iteritems()}

BARCHART_URL_DCT = {
    'signal': SIGNAL_DCT,
    'guide': GUIDE_DCT,
}

