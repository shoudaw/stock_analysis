import config
import os.path
import credentials
from scraper import Scraper
import errno


class _DFS(object):
    def __init__(self):
        self._res = []

    def _get_paths_aux(self, path, dct):
        if isinstance(dct, dict):
            for k, v in dct.iteritems():
                path.append(k)
                self._get_paths_aux(path, v)
                path.pop()
        else:
            path.append(dct)
            self._res.append(path[:])
            path.pop()

    def get_paths(self, dct):
        self._res = []
        self._get_paths_aux([], dct)
        return self._res


class Downloader(object):
    def __init__(self, url_dct, prefix=None):
        if prefix is not None:
            self.prefix = prefix
        else:
            self.prefix = ''
        self.scrapers = None
        self.session = None
        self.url_dct = url_dct

    def _prepare(self):
        if self.session is not None:
            return
        self.session = Scraper.login(credentials.USERNAME, credentials.PASSWORD)
        self._prefix_scraper_dct = {}
        paths_lst = _DFS().get_paths(self.url_dct)
        for path in paths_lst:
            prefix = os.path.join(*path[:-1])
            url = path[-1]
            scraper = Scraper(url, self.session)
            self._prefix_scraper_dct[prefix] = scraper

    def _get_path(self, prefix, scraper):
        date_str = scraper.get_date().date().isoformat()
        file_name = date_str + '.json'
        dir_path = os.path.join(config.DATA_ROOT, prefix)
        file_path = os.path.join(dir_path, file_name)
        self._mkdir_p(dir_path)
        return file_path

    def _mkdir_p(self, path):
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def save(self):
        self._prepare()
        for prefix, scraper in self._prefix_scraper_dct.iteritems():
            file_path = self._get_path(prefix, scraper)
            with open(file_path, 'w+') as f:
                json_str = scraper.get_json()
                f.write(json_str)

