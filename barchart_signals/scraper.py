import json

from dateutil import parser
import requests
import credentials

from bs4 import BeautifulSoup
import re
import copy


class Scraper(object):
    def __init__(self, url, session=None):
        self.url = url
        self._raw = None
        self._soup = None
        self.session = session
        if session is not None:
            self.session = session
        else:
            self.session = self.login(credentials.USERNAME, credentials.PASSWORD)

    def __str__(self):
        return self.url

    @classmethod
    def login(cls, username, password):
        session = requests.Session()
        login_url = 'http://www.barchart.com/login.php'
        payload = {
            'email': username,
            'password': password,
            'remember': '0',
            'submit': 'Log In',
        }
        res = session.post(login_url, payload)
        if 'Login Failure' in res.text:
            raise Exception('Login Failure')
        return session

    def refresh(self):
        self._raw = self.session.get(self.url).text
        self._soup = BeautifulSoup(self._raw, 'html5lib')

    def _prepare(self):
        if self._raw is None:
            self.refresh()

    def _get_selector_dct(self):
        default = {
            'sym': 'td.ds_symbol > a',
            'name': 'td.ds_name > a',
            'last': 'td.ds_last',
            'change': 'td.ds_change > span',
            'today_opinion': 'td.ds_opinion--op > span',
            'previous_opinion': 'td.ds_opinion--op_d > span',
            'last_week_opinion': 'td.ds_opinion--op_w > span',
            'last_month_opinion': 'td.ds_opinion--op_m > span',
        }
        guide = {
            'sym': 'td.ds_symbol > a',
            'name': 'td.ds_name > a',
            'last': 'td.ds_last',
            'change': 'td.ds_change > span',
            'today_opinion': 'td.ds_opinion--op > span',
            'short_term': 'td.ds_opinion--op_st > span',
            'med_term': 'td.ds_opinion--op_mt > span',
            'long_term': 'td.ds_opinion--op_lt > span',
        }
        up_down_grade = {
            'sym': 'td.ds_symbol > a',
            'name': 'td.ds_name > a',
            'last': 'td.ds_last',
            'change': 'td.ds_change > span',
            'today_opinion': 'td.ds_opinion--op > span',
            'previous_opinion': 'td.ds_opinion--op_d > span',
            'opinion_change': 'td.ds_opinionchg > span',
        }

        res = default
        if re.match('.*/signals/guide/((MIN)|(MAX)).*', self.url):
            is_replace = False
            if re.match('.*/signals/guide/((MIN)|(MAX))\?mode\=W.*', self.url):
                old_new_keys = [
                    ('last_week_opinion', 'previous_6_week_opinion'),
                    ('last_month_opinion', 'previous_6_month_opinion')
                ]
                is_replace = True
            elif re.match('.*/signals/guide/((MIN)|(MAX))\?mode\=M.*', self.url):
                old_new_keys = [
                    ('last_week_opinion', 'previous_12_month_opinion'),
                    ('last_month_opinion', 'previous_24_month_opinion')
                ]
                is_replace = True
            if is_replace:
                res = copy.copy(default)
                for old, new in old_new_keys:
                    v = res.pop(old)
                    res[new] = v
        elif re.match('.*/signals/((upgrades)|(downgrades)).*', self.url):
            res = up_down_grade
        elif re.match('.*/signals/guide/.*', self.url):
            res = guide

        return res

    def get_data(self):
        stock_table = self.get_stock_table()
        trs = stock_table.select('tr')
        selector_dct = self._get_selector_dct()
        res = []
        for tr in trs:
            item = {}
            for key, selector in selector_dct.iteritems():
                item[key] = tr.select(selector)[0].text
            res.append(item)

        return res

    def get_json(self):
        data = self.get_data()
        return json.dumps(data)

    def get_stock_table(self):
        self._prepare()
        selector = '#dt1 > tbody'
        return self._soup.select(selector)[0]

    def get_date(self):
        self._prepare()
        selector = '#dtaDate'
        text = self._soup.select(selector)[0].text
        date_str = ', '.join(text.split(',')[1:3])
        res = parser.parse(date_str)
        return res

