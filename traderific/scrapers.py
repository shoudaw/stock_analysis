import requests
from datetime import date
from datetime import datetime
from dateutil import parser
from bs4 import BeautifulSoup
import re
import copy
from traderific.models import TradingSignal


class TradingSignalScraper():
    URL_ROOT = 'http://traderific.com/'

    def fetch(self, dt):
        if isinstance(dt, datetime):
            dt = dt.date()
        elif not isinstance(dt, date):
            raise ValueError('dt has to be datetime.date')

        dt_str = dt.isoformat()
        payload = {
            'date': dt_str,
        }
        res = requests.get(self.URL_ROOT, params=payload)
        data = self._parse_page(res.text)
        return data

    def _parse_page(self, raw):
        soup = BeautifulSoup(raw, 'html5lib')
        dt_str = soup.select('#dailysignals > h1')[0].text
        dt = parser.parse(dt_str).date()
        daily_signals_table = soup.select('#dailysignals > table > tbody')[0]
        res = []
        line_items = daily_signals_table.select('tr[align=left]')
        for line in line_items:
            action = line.select('strong')[0].text.upper()
            action = TradingSignal.Action._member_map_[action]
            symbol = line.select('td[align=center] > a')[0].text
            entry = line.select('td[align=right]')[0].text
            target_stop_soup = line.next_sibling.next_sibling.select('td[colspan=3]')
            if target_stop_soup:
                target_stop_str = target_stop_soup[0].text
                m = re.search('Target: (\d*\.\d+|\d+) / Stop: (\d*\.\d+|\d+)', target_stop_str)
                target = float(m.group(1))
                stop = float(m.group(2))
            else:
                target = None
                stop = None
            item = {
                'date': dt,
                'symbol': symbol,
                'action': action,
                'entry': entry,
                'target': target,
                'stop': stop,
            }
            res.append(item)
        return res
