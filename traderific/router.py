_APP_LABEL = 'traderific'
_APP_DB = _APP_LABEL


class Router(object):
    """
    A router to control all database operations on models in the
    application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read models go to db.
        """
        if model._meta.app_label == _APP_LABEL:
            return _APP_DB
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write  models go to _APP_DB.
        """
        if model._meta.app_label == _APP_LABEL:
            return _APP_DB
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the app is involved.
        """
        if obj1._meta.app_label == _APP_LABEL or \
           obj2._meta.app_label == _APP_LABEL:
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the app only appears in the _APP_DB
        database.
        """
        if app_label == _APP_LABEL:
            return db == _APP_DB
        return None
