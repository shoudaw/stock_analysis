from __future__ import unicode_literals

from django.db import models
# from django_choices_enum import ChoicesEnum
from enum import Enum


class TradingSignal(models.Model):

    class Meta:
        unique_together = (('date', 'symbol'),)

    class Action(Enum):
        BUY = 'buy'
        SELL = 'sell'
        SHORT = 'short'
        COVER = 'cover'

    id = models.AutoField(primary_key=True)
    date = models.DateField(db_index=True)
    symbol = models.CharField(max_length=6, db_index=True)
    action = models.CharField(max_length=5, choices=[(tag, tag.value) for tag in Action])
    entry = models.DecimalField(max_digits=18, decimal_places=4)
    target = models.DecimalField(max_digits=18, decimal_places=4, null=True)
    stop = models.DecimalField(max_digits=18, decimal_places=4, null=True)

    def __unicode__(self):
        return '({}, {}, {})'.format(self.date.isoformat(), self.symbol, self.action)

