from django.core.management.base import BaseCommand, CommandError
from traderific.scrapers import TradingSignalScraper
from traderific.models import TradingSignal
import json
import logging
import holidays
from datetime import date
import time as sys_time
from dateutil import parser as date_parser
from bdateutil import isbday
from bdateutil import relativedelta
from datetime import datetime
from datetime import time

INTERVAL = 10
INTERVAL_STEP = 30
logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Pull traderific siginals'

    def add_arguments(self, parser):

        parser.add_argument(
            '--start-date',
            dest='start_date',
            nargs='?',
            type=lambda x: date_parser.parse(x).date(),
            help='start_date of data you want to pull, the earliest date is 2015-03-09',
        )

    def handle(self, *args, **options):
        start_date = options['start_date']
        if start_date is None:
            start_date = TradingSignal.objects.latest('date').date

        dt = start_date
        today = date.today()
        count = 0
        while dt <= today:
            count += 1
            if isbday(dt, holidays=holidays.US()):
                prev_dt = dt - relativedelta(bdays=1, holidays=holidays.US())
                self.pull_data_on_date(prev_dt)
            dt += relativedelta(bdays=1, holidays=holidays.US())
            if count % INTERVAL_STEP == 0:
                sys_time.sleep(INTERVAL)
                count = 0

    def pull_data_on_date(self, dt):
        target_dt = dt + relativedelta(bdays=1, holidays=holidays.US())
        tss = TradingSignalScraper()

        if not TradingSignal.objects.filter(date=target_dt).exists():
            try:
                data = tss.fetch(dt)
            except:
                msg = 'Failed target date: %s, generated date: %s' % (target_dt.isoformat(), dt.isoformat())
                self.stdout.write(self.style.ERROR(msg))
                logger.info(msg)
                return

            tradingSignals = [TradingSignal(**item) for item in data]
            try:
                TradingSignal.objects.bulk_create(tradingSignals)
                msg = 'Successfully pull traderific trading signals of %s' % (target_dt.isoformat())
                self.stdout.write(self.style.SUCCESS(msg))
                logger.info(msg)
            except:
                msg = 'Traderific trading signals of %s exists' % (target_dt.isoformat())
                self.stdout.write(self.style.WARNING(msg))
                logger.info(msg)
        else:
            msg = 'Traderific trading signals of %s exists' % (target_dt.isoformat())
            self.stdout.write(self.style.NOTICE(msg))
            logger.info(msg)
