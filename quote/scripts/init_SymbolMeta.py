from quote.models import OneMinuteQuote
from quote.models import SymbolMeta
import json
from traderific.models import TradingSignal


# symbols_qs = OneMinuteQuote.objects.raw('SELECT id, symbol FROM quote_oneminutequote group by symbol')
# symbols = [it.symbol for it in symbols_qs]
# symbolMetas = [SymbolMeta(symbol=symbol) for symbol in symbols]
# SymbolMeta.objects.bulk_create(symbolMetas)

# symbols_qs = TradingSignal.objects.raw('SELECT id, symbol FROM traderific_tradingsignal group by symbol')
# symbols = [it.symbol for it in symbols_qs if not SymbolMeta.objects.filter(symbol=it.symbol).exists()]
#
# symbolMetas = [SymbolMeta(symbol=symbol) for symbol in symbols]
# SymbolMeta.objects.bulk_create(symbolMetas)


symbols = json.load(open('/data/tmp/symbols.json', 'r'))
symbols = [symbol for symbol in symbols if not SymbolMeta.objects.filter(symbol=symbol).exists()]
symbolMetas = [SymbolMeta(symbol=symbol) for symbol in symbols]
SymbolMeta.objects.bulk_create(symbolMetas)

symbols_qs = TradingSignal.objects.raw('SELECT id, symbol FROM traderific_tradingsignal group by symbol')
symbols = [it.symbol for it in symbols_qs if not SymbolMeta.objects.filter(symbol=it.symbol).exists()]
SymbolMeta.objects.bulk_create(symbolMetas)

