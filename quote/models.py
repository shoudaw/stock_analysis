from __future__ import unicode_literals

from django.db import models


class OneMinuteQuote(models.Model):
    class Meta:
        unique_together = (('date', 'time', 'symbol'),)
        index_together = [
            ["symbol", "date"],
        ]
    id = models.AutoField(primary_key=True)
    date = models.DateField(db_index=True)
    time = models.TimeField(db_index=True)
    symbol = models.CharField(max_length=6, db_index=True)
    open = models.DecimalField(max_digits=18, decimal_places=4)
    high = models.DecimalField(max_digits=18, decimal_places=4)
    low = models.DecimalField(max_digits=18, decimal_places=4)
    close = models.DecimalField(max_digits=18, decimal_places=4)
    volume = models.IntegerField()


class HourlyQuote(models.Model):
    class Meta:
        unique_together = (('date', 'time', 'symbol'),)
        index_together = [
            ["symbol", "date"],
        ]
    id = models.AutoField(primary_key=True)
    date = models.DateField(db_index=True)
    time = models.TimeField(db_index=True)
    symbol = models.CharField(max_length=6, db_index=True)
    open = models.DecimalField(max_digits=18, decimal_places=4)
    high = models.DecimalField(max_digits=18, decimal_places=4)
    low = models.DecimalField(max_digits=18, decimal_places=4)
    close = models.DecimalField(max_digits=18, decimal_places=4)
    volume = models.IntegerField()


class SymbolMeta(models.Model):
    id = models.AutoField(primary_key=True)
    symbol = models.CharField(max_length=6, db_index=True, unique=True)
    created_date = models.DateField(db_index=True, auto_now_add=True)
    last_update_time = models.DateTimeField(db_index=True, auto_now_add=True)

