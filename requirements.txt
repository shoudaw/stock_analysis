Django==2.2.5
PyMySQL==0.9.3
psycopg2-binary==2.7.7
beautifulsoup4==4.4.1
html5lib==0.9999999
requests==2.10.0
python-dateutil==2.5.3
mock==2.0.0
bdateutil==0.1
django-choices-enum==0.2
django-extensions==2.1.6
six==1.12.0
mechanize==0.2.5
alpaca-trade-api
iexfinance==0.3.3
Scrapy==1.6.0
sendgrid
imgkit
mpl_finance

pandas-market-calendars
selenium
alpha_vantage
plotly

notebook>=5.3
ipywidgets>=7.2

firebase-admin==3.0.0
google-cloud-storage==1.20.0
google-cloud-tasks==1.2.1
google-cloud-logging==1.14.0
google-api-python-client==1.7.11
google-auth-httplib2==0.0.3
google-auth-oauthlib==0.4.1

fireo==1.1.5