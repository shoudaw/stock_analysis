import datetime
import time
import logging
import os
from datetime import datetime
from datetime import timedelta
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
import pandas_market_calendars as mcal
import pytz
import json

import pandas as pd
from google.cloud import storage
from google.cloud import tasks_v2
from google.protobuf import timestamp_pb2

from django.views.decorators.csrf import csrf_exempt

from stock_data.symbol_candidates import SYMBOLS
from stock_analysis.credentials import ALPACA_KEY_ID
from stock_analysis.credentials import ALPACA_SECRET_KEY
from stocktwits.models_fireo import TrendingStock
from stocktwits.clients import api as stocktwits_api


import alpaca_trade_api as tradeapi
api = tradeapi.REST(ALPACA_KEY_ID, ALPACA_SECRET_KEY, api_version='v2')

# shoudaw@gmail.com
BUCKET_NAME = "stock_analysis_data"

# johnnywsd@gmail.com
BUCKET_NAME = "stock_analysis_data_2020"


bucket = None

if os.getenv('GAE_APPLICATION', None):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(BUCKET_NAME)
else:
    bucket = None

logger = logging.getLogger(__name__)

def upload_blob(data, destination_blob_name):
    """Uploads a file to the bucket."""
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_string(data)

def file_exists(blob_name):
    blob = bucket.blob(blob_name)
    return blob.exists()


# @csrf_exempt
def pull_intraday(request, date_str):
    # date_str = request.POST['date_str']
    from dateutil.parser import parse
    today = parse(date_str)
    next_day = today + pd.Timedelta('1day')
    today_str = today.strftime("%Y-%m-%d")
    next_day_str = next_day.strftime("%Y-%m-%d")
    for symbol in SYMBOLS:
        file_key = f'daily/{today.year}/{today.month}/{today.day}/{symbol}.csv'
        if file_exists(file_key):
            continue
        intraday = api.polygon.historic_agg_v2(symbol, 1, 'minute', today_str, next_day_str)
        data = intraday.df.to_csv()
        upload_blob(data, file_key)
        logger.info(f'saved {file_key}')
        time.sleep(0.34)
    return HttpResponse("DONE")


def pull_tickers(request):
    nyse = mcal.get_calendar('NYSE')
    utc_now = pd.datetime.utcnow().astimezone(pytz.utc)
    nyse_now = utc_now.astimezone(nyse.tz)
    nyse_now_str = nyse_now.strftime("%Y%m%d-%H%M%S")
    valid_days = nyse.valid_days(nyse_now.date(), nyse_now.date())
    if valid_days.empty:
        logger.info(f"Market closed today, {nyse_now_str}")
        return HttpResponse(f"Market closed today, {nyse_now_str}")
    tickers = api.polygon.all_tickers()
    # file_key = f'all_tickers/{nyse_now.year}/{nyse_now.month}/{nyse_now.day}/{nyse_now_str}.json'
    file_key = f'all_tickers/{nyse_now.strftime("%Y/%m/%d")}/{nyse_now_str}.json'
    ticker_dict = {}
    for t in tickers:
        ticker_dict[t.ticker] = t._raw
    data = json.dumps(ticker_dict)
    if ticker_dict:
        upload_blob(data, file_key)
        logger.info(f"pull_tickers done, {nyse_now_str}")
    logger.info(f"pull_tickers empty, {nyse_now_str}")

    return HttpResponse("DONE")


def enqueue_pull_ticker(request):
    # Create a client.
    client = tasks_v2.CloudTasksClient()

    # TODO(developer): Uncomment these lines and replace with your values.
    project = settings.APP_ENGINE_PROJECT_ID
    queue = 'stockanalysis'
    location = 'us-central1'

    # Construct the fully qualified queue name.
    parent = client.queue_path(project, location, queue)

    # date_str = '2019-10-19'
    # task_id = f'pull_ticker_{date_str}'
    # task_name = f'projects/{project}/locations/{location}/queues/{queue}/tasks/{task_id}_1'
    # Construct the request body.
    task = {
        'app_engine_http_request': {  # Specify the type of request.
            'http_method': 'GET',
            'relative_uri': '/pull_tickers'
        },
        # 'name': task_name
    }

    # Use the client to build and send the task.
    response = client.create_task(parent, task)

    print('Created task {}'.format(response.name))
    return HttpResponse('Created task {}'.format(response.name))

def enqueue_pull_task(request, date_str):
    # Create a client.
    client = tasks_v2.CloudTasksClient()
    import json

    # TODO(developer): Uncomment these lines and replace with your values.
    project = settings.APP_ENGINE_PROJECT_ID
    queue = 'stockanalysis'
    location = 'us-central1'
    payload = {'date_str': '2018-10-19'}
    in_seconds = 3

    # Construct the fully qualified queue name.
    parent = client.queue_path(project, location, queue)

    date_str = '2019-10-19'
    task_id = f'pull_intraday_{date_str}'
    task_name = f'projects/{project}/locations/{location}/queues/{queue}/tasks/{task_id}_1'
    # Construct the request body.
    task = {
        'app_engine_http_request': {  # Specify the type of request.
            'http_method': 'GET',
            'relative_uri': '/pull_intraday/2019-10-19/'
        },
        'name': task_name
    }

    # if payload is not None:
    #     # The API expects a payload of type bytes.
    #     converted_payload = json.dumps(payload).encode()
    #
    #     # Add the payload to the request.
    #     task['app_engine_http_request']['body'] = converted_payload

    if in_seconds is not None:
        # Convert "seconds from now" into an rfc3339 datetime string.
        d = datetime.utcnow() + timedelta(seconds=in_seconds)

        # Create Timestamp protobuf.
        timestamp = timestamp_pb2.Timestamp()
        timestamp.FromDatetime(d)

        # Add the timestamp to the tasks.
        task['schedule_time'] = timestamp

    # Use the client to build and send the task.
    response = client.create_task(parent, task)

    print('Created task {}'.format(response.name))
    return HttpResponse('Created task {}'.format(response.name))

def pull_tasks_status(request):
    # Create a client.
    client = tasks_v2.CloudTasksClient()

    # TODO(developer): Uncomment these lines and replace with your values.
    project = settings.APP_ENGINE_PROJECT_ID
    queue = 'stockanalysis'
    location = 'us-central1'
    payload = 'hello'
    in_seconds = 3
    parent = client.queue_path(project, location, queue)
    tasks = client.list_tasks(parent)

    return render(request, 'pull_tasks_status.html', {'task_name': 'test', 'task_status': 'test_name'})

    # Construct the fully qualified queue name.
    parent = client.queue_path(project, location, queue)

    # Construct the request body.
    task = {
            'app_engine_http_request': {  # Specify the type of request.
                'http_method': 'POST',
                'relative_uri': '/pull_intraday',
                'headers': {'Content-Type': 'application/json'}
            }
    }
    payload = {'date_str': '2018-10-19'}
    import json
    if payload is not None:
        # The API expects a payload of type bytes.
        converted_payload = json.dumps(payload).encode()

        # Add the payload to the request.
        task['app_engine_http_request']['body'] = converted_payload

    import datetime
    if in_seconds is not None:
        # Convert "seconds from now" into an rfc3339 datetime string.
        d = datetime.datetime.utcnow() + datetime.timedelta(seconds=in_seconds)

        # Create Timestamp protobuf.
        timestamp = timestamp_pb2.Timestamp()
        timestamp.FromDatetime(d)

        # Add the timestamp to the tasks.
        task['schedule_time'] = timestamp

    # Use the client to build and send the task.
    response = client.create_task(parent, task)

    print('Created task {}'.format(response.name))
    return HttpResponse('Created task {}'.format(response.name))


def pull_trending_stocks(request):
    # Load the latest 30
    # compare the datetime, if recent then assign them to prev else prev = set{}
    # Fetch the current 30, get the diff, save the diff, return the diff
    TOP_N = 30
    DELTA = 10
    WINDOWW_HOURS = 1

    # start_time = dt_parser.parse("5:30am").time()
    # end_time = dt_parser.parse("1:30pm").time()
    prev = None

    # while True:
    utcnow =datetime.utcnow()
    window_left = utcnow - timedelta(hours=WINDOWW_HOURS)
    resp = stocktwits_api.get_trending_stocks()[:TOP_N]
    stock_dct = {stock_msg['symbol']: stock_msg for stock_msg in resp}
    symbols = set(stock_dct.keys())
    prev_trending_stocks = TrendingStock.collection.filter(
        'db_created_at', '>=', window_left
    ).order(
        '-db_created_at'
    ).fetch(TOP_N + DELTA)
    prev = {prev_t.symbol for prev_t in prev_trending_stocks}
    new_symbols = symbols - prev
    prev = symbols
    if new_symbols:
        for symbol in new_symbols:
            stock = stock_dct[symbol]
            trending_stock = TrendingStock()
            trending_stock.symbol = stock['symbol']
            trending_stock.title = stock['title']
            trending_stock.stock_id = stock['id']
            trending_stock.watchlist_count = stock['watchlist_count']
            trending_stock.raw = stock
            trending_stock.save()
        logger.info(f"New symbols: {new_symbols}")
    resp_text = f"new_symbols: {new_symbols}, prev: {prev}, symbols: {symbols}"
    return HttpResponse(resp_text)

def enqueue_pull_trending_stocks(request):
    # Create a client.
    client = tasks_v2.CloudTasksClient()

    project = settings.APP_ENGINE_PROJECT_ID
    queue = 'stockanalysis'
    location = 'us-central1'

    # Construct the fully qualified queue name.
    parent = client.queue_path(project, location, queue)

    task = {
        'app_engine_http_request': {  # Specify the type of request.
            'http_method': 'GET',
            'relative_uri': '/pull_trending_stocks'
        },
        # 'name': task_name
    }

    # Use the client to build and send the task.
    response = client.create_task(parent, task)

    print('Created task {}'.format(response.name))
    return HttpResponse('Created task {}'.format(response.name))