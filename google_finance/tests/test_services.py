from django.test import TestCase
from google_finance.services import GoogleIntradayQuoteService

class GoogleIntradayQuoteServiceTest(TestCase):
    def test_happy_path(self):
        symbol = 'AAPL'
        service = GoogleIntradayQuoteService(symbol=symbol)
        res = service.get_raw()
        data = service.get_quotes()
        pass
