import csv
import datetime
import requests


class GoogleDateReader(object):
    CSV_START_LINE = 7

    def __init__(self):
        pass

    def get_data(self, raw, symbol=None):
        raw_lines = raw.split()
        data = []
        if len(raw_lines) < self.CSV_START_LINE:
            # TODO: raise an error
            return data
        meta = self.get_meta(raw)
        interval_seconds = meta['interval']
        if meta['exchange'].startswith('UNKNOWN'):
            # TODO: raise an error
            return data
        csv_reader = csv.reader(raw_lines[self.CSV_START_LINE:])
        for items in csv_reader:
            if len(items) != 6:
                continue
            offset, close, high, low, open, volume = items
            if offset.startswith('a'):
                day = float(offset[1:])
                offset = 0
            else:
                offset = float(offset)
                open, high, low, close = [float(x) for x in [open, high, low, close]]
                dt = datetime.datetime.fromtimestamp(day + (interval_seconds * offset))
                dct = {
                    'date': dt.date(),
                    'time': dt.time(),
                    'open': open,
                    'high': high,
                    'low': low,
                    'close': close,
                    'volume': volume,
                }
                if symbol is not None:
                    dct['symbol'] = symbol
                data.append(dct)
        return data

    def get_meta(self, raw):
        """
        EXCHANGE%3DNYSE
        MARKET_OPEN_MINUTE=570
        MARKET_CLOSE_MINUTE=960
        INTERVAL=60
        COLUMNS=DATE,CLOSE,HIGH,LOW,OPEN,VOLUME
        DATA=
        TIMEZONE_OFFSET=-240
        """
        meta_strs = raw.split()[:self.CSV_START_LINE]
        exchange = meta_strs[0][11:]
        market_open_minute = int(meta_strs[1][meta_strs[1].find('=') + 1:])
        market_close_minute = int(meta_strs[2][meta_strs[2].find('=') + 1:])
        interval = int(meta_strs[3][meta_strs[3].find('=') + 1:])
        columns = meta_strs[4][meta_strs[4].find('=') + 1:].split(',')
        timezone_offset = int(meta_strs[6][meta_strs[6].find('=') + 1:])
        meta = {
            'exchange': exchange,
            'market_open_time': datetime.time(market_open_minute / 60, market_open_minute % 60),
            'market_close_time': datetime.time(market_close_minute / 60, market_close_minute % 60),
            'interval': interval,
            'columns': columns,
            'timezone_offset': timezone_offset
        }
        return meta


class GoogleIntradayQuoteService(object):
    ''' Intraday quotes from Google. Specify interval seconds and number of days '''
    URL = 'http://www.google.com/finance/getprices'
    reader = GoogleDateReader()

    def __init__(self, symbol, interval=60, period=1, proxies=None):
        self.symbol = symbol
        self.interval = interval
        self.period = period
        self.raw = None
        self.proxies = proxies
        pass

    def get_raw(self):
        payload = {
            'q': self.symbol,
            'i': self.interval,
            'p': '{}d'.format(self.period),
            'f': 'd,o,h,l,c,v'
        }
        try:
            res = requests.get(self.URL, params=payload, timeout=60, proxies=self.proxies)
        except:
            res = requests.get(self.URL, params=payload, timeout=60)
        self.raw = res.text
        return self.raw

    def get_quotes(self):
        if self.raw is None:
            self.get_raw()
        res = self.reader.get_data(self.raw, self.symbol)
        return res

if __name__ == '__main__':
    pass
    # q = GoogleIntradayQuote('spy',60,30)
    # print q                                           # print it out

